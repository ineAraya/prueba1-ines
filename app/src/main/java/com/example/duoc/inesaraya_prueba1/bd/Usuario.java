package com.example.duoc.inesaraya_prueba1.bd;

/**
 * Created by DUOC on 22-04-2017.
 */

public class Usuario {

    private String usario;
    private String clave;
    private String repetirClave;

    public Usuario(){}

    public Usuario(String clave, String usario, String repetirClave) {
        this.clave = clave;
        this.usario = usario;
        this.repetirClave= repetirClave;
    }

    public String getUsario() {
        return usario;
    }

    public void setUsario(String usario) {
        this.usario = usario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getRepetirClave() {
        return repetirClave;
    }

    public void setRepetirClave(String repetirClave) {
        this.repetirClave = repetirClave;
    }
}
