package com.example.duoc.inesaraya_prueba1.bd;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.duoc.inesaraya_prueba1.R;

import java.util.ArrayList;

/**
 * Created by DUOC on 22-04-2017.
 */

public class AdaptadorUsuario extends ArrayAdapter<Usuario> {

    private ArrayList<Usuario> dataSource;

    public AdaptadorUsuario(Context context, ArrayList<Usuario> dataSource){
        super(context, R.layout.activity_lista, dataSource);
        this.dataSource = dataSource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.activity_lista, null);

        TextView txNombre = (TextView) item.findViewById(R.id.txNombre);
        txNombre.setText(dataSource.get(position).getUsario());

        return (item);
    }

}
