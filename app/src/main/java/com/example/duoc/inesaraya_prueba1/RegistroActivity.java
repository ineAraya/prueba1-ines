package com.example.duoc.inesaraya_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.inesaraya_prueba1.bd.AgregarUsuario;
import com.example.duoc.inesaraya_prueba1.bd.Usuario;

public class RegistroActivity extends AppCompatActivity {

    private EditText edUsuario, etClave, etRepetirClase;
    private Button btnEnviar, btnVolver, btnListar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        edUsuario=(EditText)findViewById(R.id.edUsuario);
        etClave=(EditText)findViewById(R.id.edClave);
        etRepetirClase=(EditText)findViewById(R.id.edClave2);

        btnEnviar=(Button)findViewById(R.id.btnEnviar);

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarUsuario();

            }
        });

        btnVolver=(Button)findViewById(R.id.btnVolver);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegistroActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btnListar=(Button)findViewById(R.id.btnListar);
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(RegistroActivity.this, ListadoUsuarioActivity.class);
                startActivity(i);

            }
        });

    }

    private void guardarUsuario() {
        //String mensajeError="";
        Usuario user= new Usuario();
        user.setUsario(edUsuario.getText().toString());
        user.setClave(etClave.getText().toString());
        user.setRepetirClave(etRepetirClase.getText().toString());
        AgregarUsuario.nuevoUsuario(user);
        Toast.makeText(this, "Usuario guardado correctamente", Toast.LENGTH_SHORT).show();
       // if (edUsuario.getText().toString().length() <1);{
         //   mensajeError += "Ingrese Usuario \n";}
        //if (etClave.getText().toString().length()<1){
          //  mensajeError += "Ingrese Clave \n";}
       // else{
         //   user.setUsario(edUsuario.getText().toString());}
        //if(etRepetirClase.getText().toString() != etClave.getText().toString()){
          //  mensajeError += "Contraseña no conside \n";}

    }
}
