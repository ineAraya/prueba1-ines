package com.example.duoc.inesaraya_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.duoc.inesaraya_prueba1.bd.AdaptadorUsuario;
import com.example.duoc.inesaraya_prueba1.bd.AgregarUsuario;
import com.example.duoc.inesaraya_prueba1.bd.Usuario;

import java.util.ArrayList;

public class ListadoUsuarioActivity extends AppCompatActivity {

    private ListView lvUsuarios;
    private ArrayList<Usuario> dataSource;
    private static ArrayList<Usuario> users= AgregarUsuario.getFormulario();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuario);
        lvUsuarios= (ListView)findViewById(R.id.lvUsuario);
        AdaptadorUsuario adaptadorUsuario= new AdaptadorUsuario(this, getDataSource());

        lvUsuarios.setAdapter(adaptadorUsuario);
        lvUsuarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Toast.makeText(ListadoUsuarioActivity.this, getDataSource().get(position).getUsario(), Toast.LENGTH_LONG).show();

            }
        });

    }

    private ArrayList<Usuario> getDataSource() {
        if(dataSource == null){
            dataSource = new ArrayList<>();

            for(int x = 0; x < users.size(); x++){
                Usuario u = new Usuario(users.get(x).getUsario(), users.get(x).getClave(), users.get(x).getRepetirClave());
                dataSource.add(u);
            }
        }

        return dataSource;

    }


}
