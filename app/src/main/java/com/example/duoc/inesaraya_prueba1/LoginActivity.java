package com.example.duoc.inesaraya_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private Button btnEnviar, btnRegistrar;
    private EditText etUsuario, etClave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario = (EditText) findViewById(R.id.edUsuario);
        etClave = (EditText) findViewById(R.id.edClave);
        btnEnviar = (Button) findViewById(R.id.btnEnviar);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });

    }

    private void entrar() {

        if (etClave.getText().toString().equals("Ines") &&
                etUsuario.getText().toString().equals("1234")) {
            Toast.makeText(LoginActivity.this, "Usuario Valido", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(LoginActivity.this, "Usuario no Valido", Toast.LENGTH_SHORT).show();

               }
    }
}


